package linkedlist.singly;

import linkedlist.Node;

public class LinkedList {

    public static void printList(Node head) {
        if (head == null) {
            System.out.println("List is empty");
        }
        while (head != null) {
            System.out.format("%d\n", head.data);
            head = head.ptr;
        }
    }


    public static void addEndNode(Node head, int data) {
        Node curr, temp;

        curr = head;

        temp = new Node(data);

        while (curr.ptr != null) {
            curr = curr.ptr;
        }

        curr.ptr = temp;

    }

    public static Node addEndNodeOptimal(Node endNode, int data) {
        Node temp = new Node(data);
        endNode.ptr = temp;

        return temp;
    }

    public static Node addBeginNode(Node head, int data) {
        Node temp = new Node(data);
        temp.ptr = head;

        head = temp;

        //Need to return because we are passing a copy of head as parameter
        return head;
    }

    public static void addPosNode(Node head, int data, int pos) {
        Node temp = head;

        if (pos < 1)
            System.out.print("Invalid position");

        Node curr = new Node(data);
        curr.ptr = null;

        while (pos != 1) {
            temp = temp.ptr;
            pos--;
        }

        curr.ptr = temp.ptr;
        temp.ptr = curr;
    }

    public static void deleteEndNode(Node head) {
        //Keep two pointers, one to the last and secondlast element

        if (head == null) {
            System.out.println("List is empty");
        }

        Node temp = head;
        Node temp2 = head;

        while (temp.ptr != null) {
            temp2 = temp;
            temp = temp.ptr;

        }

        temp2.ptr = null;
        temp = null;
    }

    public static Node deleteBeginNode(Node head) {
        if (head == null) {
            System.out.println("List is empty");
        }

        Node temp = head.ptr;
        head = null;

        return temp;
    }

    public static void deletePosNode(Node head, int pos) {
        Node temp = head;
        Node temp2 = head;

        if (pos < 1)
            System.out.print("Invalid position");

        while (pos != 1) {
            temp2 = temp;
            temp = temp.ptr;
            pos--;
        }

        temp2.ptr = temp.ptr;
        temp = null;

    }

    public static void main(String[] args) {

        Node head = new Node(1);
        head.ptr = new Node(2);

        addEndNode(head, 3);

        //....optimally adding nodes.............
        Node head2 = new Node(10);
        head2.ptr = null;

        Node ptr = head2;
        ptr = addEndNodeOptimal(ptr, 20);
        ptr = addEndNodeOptimal(ptr, 30);

        printList(head2);

        System.out.println("*******************************");

        addPosNode(head2, 50, 2);

        head2 = addBeginNode(head2, 40);

//        deleteEndNode(head2);

//        head2 = deleteBeginNode(head2);

        deletePosNode(head2, 2);

        printList(head2);

    }
}
