package linkedlist.singly;

import linkedlist.Node;

public class UseCases {
    // 1. Find the middle element of a linked list in one pass
    // 2. To check for palindrome
    // 3. To check factorial


    public static int findMiddleElement(Node head) {
        // Use tortoise hare algorithm
        int length = 0;
        Node temp = head;
        Node middle = head;

        while (temp.ptr != null) {
            length++;
            if (length % 2 == 0) {
                middle = middle.ptr;
            }

            temp = temp.ptr;
        }

        if(length%2 == 1){
            middle = middle.ptr;
        }

        return middle.data;
    }

    public static void main(String[] args){

        Node head = new Node(10);
        head.ptr = new Node(50);
        head.ptr.ptr = new Node(30);


        System.out.println(findMiddleElement(head));
    }

}
