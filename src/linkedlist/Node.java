package linkedlist;

public class Node {
    public int data;
    public Node ptr;

    public Node(int data) {
        this.data = data;
    }
}
